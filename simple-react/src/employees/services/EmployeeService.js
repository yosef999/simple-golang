import client from "../../shared/http-client/Client";
import swal from "sweetalert";

export async function getEmployees(){
    const { data } = await client.get('/user');

    return data;
}

export async function getSingleEmployee(id){
    const { data } = await client.get(`/user/${id}`);

    return data;
}

export async function createEmployee(employee){
    let data;
    try {
        data = await client.post('/user', employee);
    } catch (e) {
        swal("Error!", "Data sudah ada", "error");
    }
    return data;
}

export async function updateEmployee(employee) {
    console.log(employee)
    var id = employee.user_pk;
    var professionPk = employee.user_profession_fk.toString();
    var educationPk = employee.user_education_fk.toString();
    employee.user_profession_fk = professionPk
    employee.user_education_fk = educationPk
    let data;
    try {
        data = await client.put(`/user/${id}`, employee);
    } catch (e) {
        swal("Error!", "Data sudah ada", "error");
    }
    console.log(data)
    return data;
}

export async function deleteEmployee(id) {
    const response = await client.delete(`/user/${id}`);

    if (response.status === 200) return true;
    else return false;
}

export async function getProfessions() {
    const { data } = await client.get("/profession");

    return data;
}

export async function getEducations() {
    const { data } = await client.get("/education");

    return data;
}
