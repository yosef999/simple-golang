import {
    CREATE_EMPLOYEE,
    UPDATE_EMPLOYEE,
    RESET_FORM,
    EDIT_BUTTON,
    DELETE_EMPLOYEE,
    FETCH_COMPLETE,
    SET_LOADING,
    HANDLE_INPUT_CHANGES,
    SUBMIT_COMPLETE,
    FETCH_COMPLETE_PROFESSION,
    FETCH_COMPLETE_EDUCATION
} from "./Actions";

const defaultFormValues = {
    user_pk: '',
    user_name: '',
    birth_date: '',
    nik: '',
    user_profession_fk: '',
    user_education_fk: '',

};

const initialState = {
    isLoading: true,
    employees: [],
    professions: [],
    educations: [],
    form: { ...defaultFormValues }
}
function employeeReducer(state = initialState, action) {
    const { type, payload } = action;

    switch (type) {

        case CREATE_EMPLOYEE:
            return { ...state, employees: state.employees.concat(payload)  };

        case DELETE_EMPLOYEE:
            return { ...state, employees: state.employees.filter((employee) => ( employee.user_pk !== payload))  };

        case UPDATE_EMPLOYEE:
            return { ...state, employees: state.employees.map((employee) => employee.user_pk === payload.user_pk ? payload : employee)};

        case EDIT_BUTTON:
            const employee = state.employees.find((employee)=> employee.user_pk===payload);
            return{ ...state, form: { ...employee}};

        case RESET_FORM:
            return { ...state, form: { ...defaultFormValues } }

        case SET_LOADING:
            return { ...state, isLoading: true}

        case FETCH_COMPLETE:
            return { ...state, isLoading: false, employees: [ ...payload]}

        case HANDLE_INPUT_CHANGES:
            const {form} = state;
            const {inputName, inputValue} = payload;
            form[inputName] = inputValue;

            return { ...state, form:{...form}};

        case SUBMIT_COMPLETE:
            return { ...state, isLoading: false, form: { ...defaultFormValues }};

        case FETCH_COMPLETE_PROFESSION:
            return { ...state, isLoading: false, professions: [ ...payload]}

        case FETCH_COMPLETE_EDUCATION:
            return { ...state, isLoading: false, educations: [ ...payload]}

        default:
            return { ...state }
    }
}

export default employeeReducer;
