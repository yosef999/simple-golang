package entity

type Profession struct {
	ProfessionPk int    `json:"profession_pk"`
	ProfName     string `json:"prof_name"`
}
