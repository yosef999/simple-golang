import React, { Component, Fragment } from "react";
import { Navbar, NavbarBrand } from "reactstrap";

class Header extends Component{

    render(){
        return(
            <Fragment>
                <Navbar dark color="dark" className="shadow mb-4">
                    <NavbarBrand href="/" className="mr-auto">Master Karyawan</NavbarBrand>
                </Navbar>
            </Fragment>
        )
    }
}

export default Header;
