package controller

import (
	"github.com/gorilla/mux"
	"simple-golang/service"
)

func educationController(router *mux.Router) {
	router.HandleFunc("/education", service.GetEducations).Methods("GET")
	router.HandleFunc("/education/{id}", service.GetEducation).Methods("GET")
}
