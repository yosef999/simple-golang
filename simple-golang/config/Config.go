package config
import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"simple-golang/controller"
	"simple-golang/service"
)

var db *sql.DB
var err error

func Server(){
	db, err = sql.Open("mysql", "root:@tcp(127.0.0.1:3306)/employee_go_db?parseTime=true")
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
	service.Server(db)
	controller.Controller()
}