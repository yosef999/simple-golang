package service

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"simple-golang/entity"
)

func GetEducations(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var educations []entity.Education
	result, err := db.Query("SELECT education_pk, edu_name FROM education")
	if err != nil {
		panic(err.Error())
	}
	defer result.Close()
	for result.Next() {
		var education entity.Education
		err := result.Scan(&education.EducationPk, &education.EduName)
		if err != nil {
			panic(err.Error())
		}
		educations = append(educations, education)
	}
	json.NewEncoder(w).Encode(educations)
}
func GetEducation(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	result, err := db.Query("SELECT education_pk, edu_name FROM education WHERE education_pk = ?", params["id"])
	if err != nil {
		panic(err.Error())
	}
	defer result.Close()
	var education entity.Education
	for result.Next() {
		err := result.Scan(&education.EducationPk, &education.EduName)
		if err != nil {
			panic(err.Error())
		}
	}
	json.NewEncoder(w).Encode(education)
}