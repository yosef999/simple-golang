import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import Employee from "../employees/Employee";

class Routes extends Component{
    render(){
        return(
            <Switch>
                <Route path="/" >
                    <Employee />
                </Route>
            </Switch>
        )
    }
}

export default Routes;
