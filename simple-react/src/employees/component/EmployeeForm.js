import React, { Component, Fragment } from 'react';
import {Col, Form, FormGroup, Label, Input, CardBody, Button, CardHeader, Card} from 'reactstrap';
import {
    FETCH_COMPLETE_PROFESSION,
    FETCH_COMPLETE_EDUCATION,
    HANDLE_INPUT_CHANGES,
    SET_LOADING,
    SUBMIT_COMPLETE, FETCH_COMPLETE, RESET_FORM
} from '../reducers/Actions';
import {withRouter} from 'react-router-dom';
import {createEmployee, getProfessions, getEducations, updateEmployee} from '../services/EmployeeService';
import { connect } from 'react-redux';
import swal from "sweetalert";

class EmployeeForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isSubmitting: false,
        }
    }

    loadData() {
        const { fetchData, fetchCompleteProf, fetchCompleteEdu } = this.props;

        fetchData();

        getProfessions().then((professions) => {
            fetchCompleteProf(professions);
        });

        getEducations().then((educations) => {
            fetchCompleteEdu(educations);
        });
    }

    componentDidMount() {
        this.loadData();
    }

    submitEmployeeData = async() => {
        const { form } = this.props;
        if (form.user_pk) return await updateEmployee(form);
        else return await createEmployee(form);
    }

    handleSubmit = (event) => {
        event.preventDefault();
        const { setLoading, submitComplete, history } = this.props;
        swal({
            title: "Konfirmasi",
            text: "Apakah anda akan menyimpan data ini?",
            icon: "info",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    this.setState({ isSubmitting: true });

                    setLoading();
                    this.submitEmployeeData()
                        .then((data) => {
                            submitComplete();
                            this.setState({ isSubmitting: false });
                            history.replace("/");
                        });
                    swal("Data berhasil disimpan!", {
                        icon: "success",
                    });
                } else {
                    swal("Data tidak jadi disimpan.");
                }
            });

    }

    isValid = () => {
        const { form } = this.props;

        return form.birth_date && form.nik > 0 && form.user_profession_fk > 0 && form.user_education_fk > 0;
    }

    handleReturn = () => {
        const { history, resetForm } = this.props;
        resetForm();
        history.replace("/");
    }

    generateProfession(){
        const { professions } = this.props;
        return professions.map((profession, index) => {
            return (
                <option key={profession.profession_pk} value={profession.profession_pk}>
                {profession.prof_name}</option>
            )
        })
    }

    generateEducation(){
        const { educations } = this.props;
        return educations.map((education, index) => {
            return (
                <option key={education.education_pk} value={education.education_pk}>
                    {education.edu_name}</option>
            )
        })
    }

    render() {
        const { isSubmitting } = this.state;
        const { form, isLoading, handleInputChanges} = this.props;
        return (
            <Fragment>
                <Card className="shadow">
                    <CardHeader tag="strong">Add Data Karyawan</CardHeader>
                    <CardBody>
                        <Form onSubmit={(event) => this.handleSubmit(event)}>
                            <FormGroup row>
                                    <Label for="user_name" sm="3">Nama</Label>
                                    <Col sm="9">
                                        <Input type="text" id="user_name" name="user_name" value={form.user_name} onChange={(event)=>handleInputChanges('user_name', event.target.value)} required={true}/>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label for="birth_date" sm="3">Tanggal Lahir</Label>
                                    <Col sm="9">
                                        <Input type="date" id="birth_date" name="birth_date" value={form.birth_date} onChange={(event)=>handleInputChanges('birth_date', event.target.value)}/>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label for="nik" sm="3">Nomor KTP</Label>
                                    <Col sm="9">
                                        <Input type="number" id="nik" name="nik" value={form.nik} onChange={(event)=>handleInputChanges('nik', event.target.value)}/>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label for="user_profession_fk" sm="3">Pekerjaan</Label>
                                    <Col sm="9">
                                        <Input type="select" id="user_profession_fk" name="user_profession_fk" value={form.user_profession_fk} onChange={(event)=>handleInputChanges('user_profession_fk', event.target.value)}>
                                            <option default>== Pilih ==</option>
                                            {this.generateProfession()}
                                        </Input>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label for="user_education_fk" sm="3">Pendidikan Terakhir</Label>
                                    <Col sm="9">
                                        <Input type="select" id="user_education_fk" name="user_education_fk" value={form.user_education_fk} onChange={(event)=>handleInputChanges('user_education_fk', event.target.value)}>
                                            <option default>== Pilih ==</option>
                                            {this.generateEducation()}
                                        </Input>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Col sm={{size:9, offset: 3}}>
                                        <Button color="primary" type="submit" disabled={!this.isValid() || isSubmitting}>
                                             {!isLoading ? 'Save Employee' : 'Submitting data...'}
                                        </Button>
                                        <Button type="reset" color="secondary" onClick={this.handleReturn} className="ml-3">return   </Button>
                                    </Col>
                                </FormGroup>
                            </Form>
                        </CardBody>
                </Card>
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {...state};
}

function mapDispatchToProps(dispatch) {
    return {
        fetchData: () => dispatch({type: SET_LOADING}),
        fetchComplete: (payload) => dispatch({ type: FETCH_COMPLETE, payload }),
        fetchCompleteProf: (payload) => dispatch({ type: FETCH_COMPLETE_PROFESSION, payload }),
        fetchCompleteEdu: (payload) => dispatch({ type: FETCH_COMPLETE_EDUCATION, payload }),
        handleInputChanges: (inputName, inputValue) => dispatch({type: HANDLE_INPUT_CHANGES, payload : { inputName, inputValue}}),
        setLoading: () => dispatch({ type : SET_LOADING}),
        submitComplete: () => dispatch({ type: SUBMIT_COMPLETE}),
        resetForm: () => dispatch({type: RESET_FORM})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(EmployeeForm));
