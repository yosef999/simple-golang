package controller

import (
	"github.com/gorilla/mux"
	"simple-golang/service"
)

func userController(router *mux.Router) {
	router.HandleFunc("/user", service.GetUsers).Methods("GET")
	router.HandleFunc("/user", service.CreateUser).Methods("POST")
	router.HandleFunc("/user/{id}", service.GetUser).Methods("GET")
	router.HandleFunc("/user/{id}", service.UpdateUser).Methods("PUT")
	router.HandleFunc("/user/{id}", service.DeleteUser).Methods("DELETE")
}
