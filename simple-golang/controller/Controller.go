package controller

import (
	"github.com/gorilla/mux"
	"net/http"
)

func Controller() {
	router := mux.NewRouter()
	userController(router)
	educationController(router)
	professionController(router)
	http.ListenAndServe(":8000", router)

}