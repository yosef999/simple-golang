package entity

import "time"

type User struct {
	UserPk           int        `json:"user_pk"`
	UserName         string     `json:"user_name"`
	BirthDate        *time.Time `json:"birth_date"`
	NIK              string     `json:"nik"`
	Profession       []Profession
	Education        []Education
	UserProfessionFk int    `json:"user_profession_fk"`
	UserEducationFk  int    `json:"user_education_fk"`

}