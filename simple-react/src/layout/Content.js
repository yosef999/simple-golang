import React, { Component, Fragment } from "react";
import { Row, Col } from "reactstrap";
import Header from "./Header";
import Routes from "../routes/Routes";


class Content extends Component {
    render() {
        return(
            <Fragment>
                <Row>
                    <Col sm="12" className="p-0">
                        <Header />
                    </Col>
                </Row>
                <Row>
                    <Col sm="12">
                        <Routes />
                    </Col>
                </Row>
            </Fragment>
        );
    }
}

export default Content;
