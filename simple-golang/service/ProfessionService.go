package service

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"simple-golang/entity"
)

func GetProfessions(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var professions []entity.Profession
	result, err := db.Query("SELECT profession_pk, prof_name FROM profession")
	if err != nil {
		panic(err.Error())
	}
	defer result.Close()
	for result.Next() {
		var profession entity.Profession
		err := result.Scan(&profession.ProfessionPk, &profession.ProfName)
		if err != nil {
			panic(err.Error())
		}
		professions = append(professions, profession)
	}
	json.NewEncoder(w).Encode(professions)
}
func GetProfession(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	result, err := db.Query("SELECT profession_pk, prof_name FROM profession WHERE profession_pk = ?", params["id"])
	if err != nil {
		panic(err.Error())
	}
	defer result.Close()
	var profession entity.Profession
	for result.Next() {
		err := result.Scan(&profession.ProfessionPk, &profession.ProfName)
		if err != nil {
			panic(err.Error())
		}
	}
	json.NewEncoder(w).Encode(profession)
}