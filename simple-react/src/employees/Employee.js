import React, { Component } from "react";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { Row, Col } from "reactstrap";
import employeeReducer from "./reducers/EmployeeReducer";
import ListEmployee from "./component/ListEmployee";
import { Route } from "react-router-dom";
import EmployeeForm from "./component/EmployeeForm";

const employeeStore = createStore(employeeReducer);
class Employee extends Component{
    render(){

        return(
            <Provider store={employeeStore}>
                <Row>
                    <Col>
                        <Route exact path="/" render={() => <ListEmployee/>}/>
                        <Route path="/form" render={() => <EmployeeForm/>}/>
                    </Col>
                </Row>
            </Provider>
        )
    }
}

export default Employee;
