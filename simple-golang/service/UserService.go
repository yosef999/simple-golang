package service

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"simple-golang/entity"
)


func GetUsers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var users []entity.User
	result, err := db.Query(
		"SELECT * FROM user T1 " +
			"JOIN profession T2 ON T1.user_profession_fk = T2.profession_pk " +
			"JOIN education T3 ON T1.user_education_fk = T3.education_pk")
	if err != nil {
		panic(err.Error())
	}
	defer result.Close()
	for result.Next() {
		var user entity.User
		var profession entity.Profession
		var education entity.Education
		err := result.Scan(
			&user.UserPk,
			&user.UserName,
			&user.BirthDate,
			&user.NIK,
			&user.UserProfessionFk,
			&user.UserEducationFk,
			&profession.ProfessionPk,
			&profession.ProfName,
			&education.EducationPk,
			&education.EduName)
		if err != nil {
			panic(err.Error())
		}
		user.Profession = append(user.Profession, profession)
		user.Education = append(user.Education, education)
		users = append(users, user)
	}

	json.NewEncoder(w).Encode(users)
}
func CreateUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	stmt, err := db.Prepare("INSERT INTO user(user_name, birth_date, nik, user_profession_fk, user_education_fk) VALUES(?,?,?,?,?)")
	if err != nil {
		panic(err.Error())
	}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err.Error())
	}
	keyVal := make(map[string]string)
	json.Unmarshal(body, &keyVal)
	userName			:= keyVal["user_name"]
	birthDate			:= keyVal["birth_date"]
	nik					:= keyVal["nik"]
	UserProfessionFk	:= keyVal["user_profession_fk"]
	UserEducationFk		:= keyVal["user_education_fk"]
	_, err = stmt.Exec(userName, birthDate, nik, UserProfessionFk, UserEducationFk)
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintf(w, "New user was created")
}
func GetUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	result, err := db.Query(
		"SELECT * FROM user T1 " +
			"JOIN profession T2 ON T1.user_profession_fk = T2.profession_pk " +
			"JOIN education T3 ON T1.user_education_fk = T3.education_pk " +
			"WHERE user_pk = ?", params["id"])
	if err != nil {
		panic(err.Error())
	}
	defer result.Close()
	var user entity.User
	var profession entity.Profession
	var education entity.Education
	for result.Next() {
		err := result.Scan(
			&user.UserPk,
			&user.UserName,
			&user.BirthDate,
			&user.NIK,
			&user.UserProfessionFk,
			&user.UserEducationFk,
			&profession.ProfessionPk,
			&profession.ProfName,
			&education.EducationPk,
			&education.EduName)
		if err != nil {
			panic(err.Error())
		}
	}
	user.Profession = append(user.Profession, profession)
	user.Education = append(user.Education, education)
	json.NewEncoder(w).Encode(user)
}
func UpdateUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	stmt, err := db.Prepare("UPDATE user SET user_name = ?, birth_date = ?, nik = ?, user_profession_fk = ?, user_education_fk = ? WHERE user_pk = ?")
	if err != nil {
		panic(err.Error())
	}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err.Error())
	}
	keyVal := make(map[string]string)
	json.Unmarshal(body, &keyVal)
	userName			:= keyVal["user_name"]
	birthDate			:= keyVal["birth_date"]
	nik					:= keyVal["nik"]
	UserProfessionFk	:= keyVal["user_profession_fk"]
	UserEducationFk		:= keyVal["user_education_fk"]
	_, err = stmt.Exec(userName, birthDate, nik, UserProfessionFk, UserEducationFk, params["id"])
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintf(w, "User with ID = %s was updated", params["id"])
}
func DeleteUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	stmt, err := db.Prepare("DELETE FROM user WHERE user_pk = ?")
	if err != nil {
		panic(err.Error())
	}
	_, err = stmt.Exec(params["id"])
	if err != nil {
		panic(err.Error())
	}
	fmt.Fprintf(w, "User with ID = %s was deleted", params["id"])
}
