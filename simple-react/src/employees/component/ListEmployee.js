import React, { Component } from "react";
import { Table, Card, CardHeader, Spinner, Button } from "reactstrap";
import {
    FETCH_COMPLETE,
    SET_LOADING,
    EDIT_BUTTON,
    FETCH_COMPLETE_PROFESSION,
    SUBMIT_COMPLETE, RESET_FORM
} from "../reducers/Actions";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import { getEmployees, deleteEmployee } from "../services/EmployeeService";
import swal from 'sweetalert';

class ListEmployee extends Component{
    loadData() {
        const { fetchData, fetchComplete } = this.props;

        fetchData();

        getEmployees().then((employees) => {
            fetchComplete(employees);
        });
    }

    handleEdit = (employeeId) => {
        const { handleEdit, history } = this.props;
        handleEdit(employeeId);
        history.replace("/form");
    }

    handleDelete = (employeeId, name) => {
        swal({
            title: "Konfirmasi",
            text: "Apakah anda akan menghapus data ini?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    deleteEmployee(employeeId)
                        .then((isSuccess) => {
                            if (isSuccess) this.loadData();
                        });
                    swal("Data berhasil dihapus!", {
                        icon: "success",
                    });
                } else {
                    swal("Data tidak jadi dihapus.");
                }
            });
    }

    componentDidMount() {
        this.loadData();
    }

    generateTableRows(){
        const { employees } = this.props;
        let rows = <tr><td colSpan="7" className="text-center"> <Spinner type="grow" color="primary" /></td></tr>

        if(!this.props.isLoading){
            let num = 1;
            rows = employees.map((employee, index) => {
                const profession = employee.Profession;
                const education = employee.Education;
                const userPk = employee.user_pk;
                const birthDate = new Date(employee.birth_date).toLocaleDateString();
                return(
                    <tr key={num}>
                        <td>{num++}</td>
                        <td>{employee.user_name}</td>
                        <td>{birthDate}</td>
                        <td>{employee.nik}</td>
                        {profession.map(prof => <td>{prof.prof_name}</td>)}
                        {education.map(edu => <td>{edu.edu_name}</td>)}
                        <td>
                            <Button type="button" color="warning" size="sm" className="shadow" onClick={() =>this.handleEdit(userPk)}>
                                Edit
                            </Button>
                        </td>
                        <td>
                            <Button type="button" color="danger" size="sm" className="shadow" onClick={() => this.handleDelete(userPk, employee.user_name)}>
                                Delete
                            </Button>
                        </td>
                    </tr>
                )
            });
        }
        return rows;
    }

    render(){
        return(
            <Card className="shadow">

                <CardHeader tag="strong">
                    List Karyawan<Link to="/form"><Button color="primary" className="float-right" size="sm">Add Employee</Button></Link>
                </CardHeader>
                <Table responsive striped hover className="m-0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Tanggal Lahir</th>
                        <th>No KTP</th>
                        <th>Pekerjaan</th>
                        <th>Pendidikan Terakhir</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    { this.generateTableRows() }
                    </tbody>
                </Table>
            </Card>
        );
    }

}

function mapStateToProps(state) {
    return { ...state };
}

function mapDispatchToProps(dispatch) {
    return {
        fetchCompleteProf: (payload) => dispatch({ type: FETCH_COMPLETE_PROFESSION, payload }),
        fetchData: () => dispatch({type: SET_LOADING}),
        fetchComplete: (payload) => dispatch({ type: FETCH_COMPLETE, payload }),
        handleEdit: (payload) => dispatch({ type: EDIT_BUTTON, payload }),
        submitComplete: () => dispatch({ type: SUBMIT_COMPLETE}),
        resetForm: () => dispatch({type: RESET_FORM})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ListEmployee));
