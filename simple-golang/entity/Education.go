package entity

type Education struct {
	EducationPk int    `json:"education_pk"`
	EduName     string `json:"edu_name"`
}