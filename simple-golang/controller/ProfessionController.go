package controller

import (
	"github.com/gorilla/mux"
	"simple-golang/service"
)

func professionController(router *mux.Router) {
	router.HandleFunc("/profession", service.GetProfessions).Methods("GET")
	router.HandleFunc("/profession/{id}", service.GetProfession).Methods("GET")
}
