import React, { Component } from 'react';
import { Container } from 'reactstrap';
import Content from './layout/Content';
import { BrowserRouter as Router } from "react-router-dom";

class App extends Component{
  render(){

    return(
        <Container fluid>
          <Router>
            <Content />
          </Router>
        </Container>
    );
  }
}

export default App;
